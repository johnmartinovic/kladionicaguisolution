﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Xml;
using MySql.Data.MySqlClient;

namespace KladionicaGUI
{
    public partial class StartUpWindow : Form
    {
        public StartUpWindow()
        {
            InitializeComponent();
        }

        private void StartUpWindow_Load(object sender, EventArgs e)
        {
            InitStartUpWindow();
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            // provjera odabranih pathova. Izadi ako ijedan nije dobar
            if (!Directory.Exists(this.textBoxDBPath.Text) ||
                !File.Exists(this.textBoxPythonPath.Text))
            {
                MessageBox.Show("Jedan od odabranih pathova ne postoji.", "Greška", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.bw.RunWorkerAsync();
        }

        private void PythonPathBrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.textBoxPythonPath.Text = ofd.FileName;
            else
                MessageBox.Show("Niste odabrali file.", "Ponovi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void HTMLponudaBrowseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog ofd = new FolderBrowserDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.textBoxDBPath.Text = ofd.SelectedPath;
            else
                MessageBox.Show("Niste odabrali folder.", "Ponovi", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void satiPicker_Leave(object sender, EventArgs e)
        {
            if (satiPicker.Text.Equals(""))
            {
                satiPicker.Text = "0";
                satiPicker.Value = 0;
            } 
        }

        private void minutePicker_Leave(object sender, EventArgs e)
        {
            if (minutePicker.Text.Equals(""))
            {
                minutePicker.Text = "0";
                minutePicker.Value = 0;
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cs =     @"Server=46.101.137.168;
                            Uid=ordinaryuser;
                            Pwd=mypass01;
                            Database=KladionicaGlavnaBaza;";

            MySqlConnection conn = null;
            MySqlDataReader rdr = null;

            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();

                string stm = "SHOW TABLES;";
                MySqlCommand cmd = new MySqlCommand(stm, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    textBox1.Text += rdr.GetString(0) + "\n";
                }
            }
            catch (MySqlException ex)
            {
                textBox1.Text = ex.Message;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
        
    }
}
