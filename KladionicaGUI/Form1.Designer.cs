﻿namespace KladionicaGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.minGamesPicker = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.filterOdds2TextBox = new System.Windows.Forms.TextBox();
            this.filterOddsXTextBox = new System.Windows.Forms.TextBox();
            this.filterOdds1TextBox = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.toDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.fromDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.OddsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.CheckUnCheck = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.checkAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uncheckAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.MatchListView = new System.Windows.Forms.ListView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.StatsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.SportsLeaguesTreeView = new System.Windows.Forms.TreeView();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.button3Korak = new System.Windows.Forms.Button();
            this.button4Korak = new System.Windows.Forms.Button();
            this.button2Korak = new System.Windows.Forms.Button();
            this.buttonObradiBazu = new System.Windows.Forms.Button();
            this.labelSteps = new System.Windows.Forms.Label();
            this.picSteps = new System.Windows.Forms.PictureBox();
            this.buttonClearOddsOpt = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minGamesPicker)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OddsObjectListView)).BeginInit();
            this.CheckUnCheck.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StatsObjectListView)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSteps)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonClearOddsOpt);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(430, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(119, 158);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Odds options";
            // 
            // groupBox4
            // 
            this.groupBox4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox4.Controls.Add(this.minGamesPicker);
            this.groupBox4.Location = new System.Drawing.Point(6, 70);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(107, 45);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Min. games";
            // 
            // minGamesPicker
            // 
            this.minGamesPicker.Location = new System.Drawing.Point(6, 19);
            this.minGamesPicker.Name = "minGamesPicker";
            this.minGamesPicker.Size = new System.Drawing.Size(95, 20);
            this.minGamesPicker.TabIndex = 10;
            this.minGamesPicker.Leave += new System.EventHandler(this.minGamesPicker_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox3.Controls.Add(this.filterOdds2TextBox);
            this.groupBox3.Controls.Add(this.filterOddsXTextBox);
            this.groupBox3.Controls.Add(this.filterOdds1TextBox);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(107, 45);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filter odds";
            // 
            // filterOdds2TextBox
            // 
            this.filterOdds2TextBox.Location = new System.Drawing.Point(72, 19);
            this.filterOdds2TextBox.Name = "filterOdds2TextBox";
            this.filterOdds2TextBox.Size = new System.Drawing.Size(27, 20);
            this.filterOdds2TextBox.TabIndex = 2;
            // 
            // filterOddsXTextBox
            // 
            this.filterOddsXTextBox.Location = new System.Drawing.Point(39, 19);
            this.filterOddsXTextBox.Name = "filterOddsXTextBox";
            this.filterOddsXTextBox.Size = new System.Drawing.Size(27, 20);
            this.filterOddsXTextBox.TabIndex = 1;
            // 
            // filterOdds1TextBox
            // 
            this.filterOdds1TextBox.Location = new System.Drawing.Point(6, 19);
            this.filterOdds1TextBox.Name = "filterOdds1TextBox";
            this.filterOdds1TextBox.Size = new System.Drawing.Size(27, 20);
            this.filterOdds1TextBox.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.AutoSize = true;
            this.groupBox9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox9.Controls.Add(this.toDateTimePicker);
            this.groupBox9.Location = new System.Drawing.Point(12, 145);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(108, 57);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "To date...";
            // 
            // toDateTimePicker
            // 
            this.toDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.toDateTimePicker.Location = new System.Drawing.Point(7, 18);
            this.toDateTimePicker.Name = "toDateTimePicker";
            this.toDateTimePicker.Size = new System.Drawing.Size(95, 20);
            this.toDateTimePicker.TabIndex = 0;
            this.toDateTimePicker.Value = new System.DateTime(2015, 4, 6, 0, 0, 0, 0);
            // 
            // groupBox8
            // 
            this.groupBox8.AutoSize = true;
            this.groupBox8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox8.Controls.Add(this.fromDateTimePicker);
            this.groupBox8.Location = new System.Drawing.Point(12, 82);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(108, 57);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "From date...";
            // 
            // fromDateTimePicker
            // 
            this.fromDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fromDateTimePicker.Location = new System.Drawing.Point(7, 18);
            this.fromDateTimePicker.Name = "fromDateTimePicker";
            this.fromDateTimePicker.Size = new System.Drawing.Size(95, 20);
            this.fromDateTimePicker.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox5.Controls.Add(this.OddsObjectListView);
            this.groupBox5.Location = new System.Drawing.Point(555, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(357, 190);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Odds";
            // 
            // OddsObjectListView
            // 
            this.OddsObjectListView.ContextMenuStrip = this.CheckUnCheck;
            this.OddsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OddsObjectListView.Location = new System.Drawing.Point(3, 16);
            this.OddsObjectListView.Name = "OddsObjectListView";
            this.OddsObjectListView.Size = new System.Drawing.Size(351, 171);
            this.OddsObjectListView.TabIndex = 0;
            this.OddsObjectListView.UseCompatibleStateImageBehavior = false;
            this.OddsObjectListView.View = System.Windows.Forms.View.Details;
            // 
            // CheckUnCheck
            // 
            this.CheckUnCheck.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkAllToolStripMenuItem,
            this.uncheckAllToolStripMenuItem});
            this.CheckUnCheck.Name = "sportLeaguesCheck";
            this.CheckUnCheck.Size = new System.Drawing.Size(138, 48);
            // 
            // checkAllToolStripMenuItem
            // 
            this.checkAllToolStripMenuItem.Name = "checkAllToolStripMenuItem";
            this.checkAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.checkAllToolStripMenuItem.Text = "Check All";
            this.checkAllToolStripMenuItem.Click += new System.EventHandler(this.checkAllToolStripMenuItem_Click);
            // 
            // uncheckAllToolStripMenuItem
            // 
            this.uncheckAllToolStripMenuItem.Name = "uncheckAllToolStripMenuItem";
            this.uncheckAllToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.uncheckAllToolStripMenuItem.Text = "Uncheck All";
            this.uncheckAllToolStripMenuItem.Click += new System.EventHandler(this.uncheckAllToolStripMenuItem_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.MatchListView);
            this.groupBox6.Location = new System.Drawing.Point(12, 208);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(675, 345);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Match list";
            // 
            // MatchListView
            // 
            this.MatchListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MatchListView.Location = new System.Drawing.Point(3, 16);
            this.MatchListView.Name = "MatchListView";
            this.MatchListView.Size = new System.Drawing.Size(669, 326);
            this.MatchListView.TabIndex = 2;
            this.MatchListView.UseCompatibleStateImageBehavior = false;
            this.MatchListView.View = System.Windows.Forms.View.Details;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.StatsObjectListView);
            this.groupBox7.Location = new System.Drawing.Point(693, 208);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(219, 345);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Statistics";
            // 
            // StatsObjectListView
            // 
            this.StatsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatsObjectListView.Location = new System.Drawing.Point(3, 16);
            this.StatsObjectListView.Name = "StatsObjectListView";
            this.StatsObjectListView.Size = new System.Drawing.Size(213, 326);
            this.StatsObjectListView.TabIndex = 0;
            this.StatsObjectListView.UseCompatibleStateImageBehavior = false;
            this.StatsObjectListView.View = System.Windows.Forms.View.Details;
            // 
            // groupBox10
            // 
            this.groupBox10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox10.Controls.Add(this.SportsLeaguesTreeView);
            this.groupBox10.Location = new System.Drawing.Point(204, 12);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(220, 190);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Sport/Leagues";
            // 
            // SportsLeaguesTreeView
            // 
            this.SportsLeaguesTreeView.ContextMenuStrip = this.CheckUnCheck;
            this.SportsLeaguesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SportsLeaguesTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SportsLeaguesTreeView.Location = new System.Drawing.Point(3, 16);
            this.SportsLeaguesTreeView.Name = "SportsLeaguesTreeView";
            this.SportsLeaguesTreeView.Size = new System.Drawing.Size(214, 171);
            this.SportsLeaguesTreeView.TabIndex = 0;
            this.SportsLeaguesTreeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.SportsLeaguesTreeView_AfterCheck);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // button3Korak
            // 
            this.button3Korak.Location = new System.Drawing.Point(453, 176);
            this.button3Korak.Name = "button3Korak";
            this.button3Korak.Size = new System.Drawing.Size(72, 26);
            this.button3Korak.TabIndex = 4;
            this.button3Korak.Text = "Step 3";
            this.button3Korak.UseVisualStyleBackColor = true;
            this.button3Korak.Click += new System.EventHandler(this.button3Korak_Click);
            // 
            // button4Korak
            // 
            this.button4Korak.Location = new System.Drawing.Point(918, 176);
            this.button4Korak.Name = "button4Korak";
            this.button4Korak.Size = new System.Drawing.Size(72, 26);
            this.button4Korak.TabIndex = 4;
            this.button4Korak.Text = "Step 4";
            this.button4Korak.UseVisualStyleBackColor = true;
            this.button4Korak.Click += new System.EventHandler(this.button4Korak_Click);
            // 
            // button2Korak
            // 
            this.button2Korak.Location = new System.Drawing.Point(126, 176);
            this.button2Korak.Name = "button2Korak";
            this.button2Korak.Size = new System.Drawing.Size(72, 26);
            this.button2Korak.TabIndex = 5;
            this.button2Korak.Text = "Step 2";
            this.button2Korak.UseVisualStyleBackColor = true;
            this.button2Korak.Click += new System.EventHandler(this.button2Korak_Click);
            // 
            // buttonObradiBazu
            // 
            this.buttonObradiBazu.Location = new System.Drawing.Point(12, 12);
            this.buttonObradiBazu.Name = "buttonObradiBazu";
            this.buttonObradiBazu.Size = new System.Drawing.Size(111, 26);
            this.buttonObradiBazu.TabIndex = 7;
            this.buttonObradiBazu.Text = "Refresh DB";
            this.buttonObradiBazu.UseVisualStyleBackColor = true;
            this.buttonObradiBazu.Click += new System.EventHandler(this.buttonObradiBazu_Click);
            // 
            // labelSteps
            // 
            this.labelSteps.AutoSize = true;
            this.labelSteps.Location = new System.Drawing.Point(34, 45);
            this.labelSteps.Name = "labelSteps";
            this.labelSteps.Size = new System.Drawing.Size(89, 13);
            this.labelSteps.TabIndex = 9;
            this.labelSteps.Text = "Creating folders...";
            // 
            // picSteps
            // 
            this.picSteps.Location = new System.Drawing.Point(12, 44);
            this.picSteps.Name = "picSteps";
            this.picSteps.Size = new System.Drawing.Size(16, 16);
            this.picSteps.TabIndex = 8;
            this.picSteps.TabStop = false;
            // 
            // buttonClearOddsOpt
            // 
            this.buttonClearOddsOpt.Location = new System.Drawing.Point(23, 121);
            this.buttonClearOddsOpt.Name = "buttonClearOddsOpt";
            this.buttonClearOddsOpt.Size = new System.Drawing.Size(72, 26);
            this.buttonClearOddsOpt.TabIndex = 10;
            this.buttonClearOddsOpt.Text = "Clear opts";
            this.buttonClearOddsOpt.UseVisualStyleBackColor = true;
            this.buttonClearOddsOpt.Click += new System.EventHandler(this.buttonClearOddsOpt_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 565);
            this.Controls.Add(this.labelSteps);
            this.Controls.Add(this.picSteps);
            this.Controls.Add(this.buttonObradiBazu);
            this.Controls.Add(this.button2Korak);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.button4Korak);
            this.Controls.Add(this.button3Korak);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "KladaLAB";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.minGamesPicker)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OddsObjectListView)).EndInit();
            this.CheckUnCheck.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StatsObjectListView)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSteps)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox filterOdds1TextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DateTimePicker fromDateTimePicker;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DateTimePicker toDateTimePicker;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ListView MatchListView;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TreeView SportsLeaguesTreeView;
        private System.Windows.Forms.ContextMenuStrip CheckUnCheck;
        private System.Windows.Forms.ToolStripMenuItem checkAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uncheckAllToolStripMenuItem;
        private System.Windows.Forms.Button button3Korak;
        private System.Windows.Forms.Button button4Korak;
        private System.Windows.Forms.Button button2Korak;
        private System.Windows.Forms.Button buttonObradiBazu;
        private System.Windows.Forms.Label labelSteps;
        private System.Windows.Forms.PictureBox picSteps;
        private System.Windows.Forms.TextBox filterOdds2TextBox;
        private System.Windows.Forms.TextBox filterOddsXTextBox;
        private System.Windows.Forms.NumericUpDown minGamesPicker;
        private BrightIdeasSoftware.ObjectListView OddsObjectListView;
        private BrightIdeasSoftware.ObjectListView StatsObjectListView;
        private System.Windows.Forms.Button buttonClearOddsOpt;

    }
}

