﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32.TaskScheduler;

namespace KladionicaGUI
{
    partial class StartUpWindow
    {
        /// <summary>
        /// Globalne varijable
        /// </summary>

        // path do XML dokumenta s postavkama programa
        string XMLPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) +
                "\\KladionicaDATA.xml";

        // path do programa koji sluzi za skidanje podataka kako
        // bi se mogao staviti u Windows Task Scheduler
        string downloadingEXEPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath) +
                "\\KladionicaDataDownloader.exe";

        // ime Task koji ce se ubaciti u Windows Task Scheduler i koji ce sluziti za skidanje podataka
        string imeTaska = "KladionicaSkidanjeTask";

        // globalne varijable koje su izvucene iz XML-a s postavkama programa
        // pri pokretanju ovog prozora
        string globalPythonPath = "";
        string globalDatabasePath = "";
        string globalExeSati = "";
        string globalExeMinute = "";
        
        // BackgroundWorker koji ce sluziti za skidanje podataka s interneta
        BackgroundWorker bw;

        /// <summary>
        /// Osnovne postavke ovog radnog prozora.
        /// </summary>
        void InitStartUpWindow()
        {
            // sakrij slike i labele na pocetku
            this.picStep1.Visible = false;
            this.picStep2.Visible = false;
            this.picStep3.Visible = false;
            this.picStep4.Visible = false;
            this.labelStep1.Visible = false;
            this.labelStep2.Visible = false;
            this.labelStep3.Visible = false;
            this.labelStep4.Visible = false;

            // onemoguci textboxove
            this.textBoxDBPath.Enabled = false;
            this.textBoxPythonPath.Enabled = false;

            // postavi vrsne vrijednosti kucica za odabiranje vremena skidanja podataka
            this.satiPicker.Minimum = 0;
            this.satiPicker.Maximum = 23;
            this.minutePicker.Minimum = 0;
            this.minutePicker.Maximum = 59;

            BWinitialization();
            
            // provjera postoji li vec definiran XML dokument s pathovima                        
            if (File.Exists(this.XMLPath))
            {
                try
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(this.XMLPath);
                    XmlNodeList DatabasePathNodes = xDoc.SelectNodes("/DataList/Data[@AttName='DatabasePath']");
                    XmlNodeList PythonPathNodes = xDoc.SelectNodes("/DataList/Data[@AttName='PythonPath']");
                    XmlNodeList ExeSatiNodes = xDoc.SelectNodes("/DataList/Data[@AttName='ExeSati']");
                    XmlNodeList ExeMinuteNodes = xDoc.SelectNodes("/DataList/Data[@AttName='ExeMinute']");
                    
                    // provjera postoji li samo jedna vrijednost za svaki attribute. Inace file
                    // smatramo nevaljalim
                    if (DatabasePathNodes.Count == 1 && 
                        PythonPathNodes.Count == 1 &&
                        ExeSatiNodes.Count == 1 &&
                        ExeMinuteNodes.Count == 1)
                    {
                        this.textBoxDBPath.Text = DatabasePathNodes[0].InnerText;
                        this.textBoxPythonPath.Text = PythonPathNodes[0].InnerText;
                        this.satiPicker.Text = ExeSatiNodes[0].InnerText;
                        this.minutePicker.Text = ExeMinuteNodes[0].InnerText;
                        this.globalDatabasePath = this.textBoxDBPath.Text;
                        this.globalPythonPath = this.textBoxPythonPath.Text;
                        this.globalExeSati = this.satiPicker.Text;
                        this.globalExeMinute = this.minutePicker.Text;
                    }                        
                }
                catch (XmlException e1)
                {
                    // samo prodi
                }
            }

        }

        /// <summary>
        /// Inicijalizacija BackgroundWorkera za skidanje podataka s interneta
        /// </summary>
        void BWinitialization()
        {
            this.bw = new BackgroundWorker();

            // this allows our worker to report progress during work
            this.bw.WorkerReportsProgress = true;

            // what to do in the background thread
            this.bw.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    BackgroundWorker b = o as BackgroundWorker;

                    b.ReportProgress(0, "Izrada foldera...");

                    checkFolders();

                    Thread.Sleep(1000);
                    b.ReportProgress(1, "Spremanje postavki...");

                    XMLDataSave();

                    Thread.Sleep(1000);
                    b.ReportProgress(2, "Inicijalizacija taska...");

                    createWindowsTask();

                    Thread.Sleep(1000);
                    b.ReportProgress(3, "Lansiranje!!!");

                    Thread.Sleep(1000);

                    b.ReportProgress(4, "Kreni na sljedeci prozor, a ovaj zatvori...");
                });

            this.bw.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    string faza = args.UserState as string;

                    if (faza == "Izrada foldera...")
                    {
                        // postavi prvu fazu slika
                        this.picStep1.Image = Properties.Resources.Loading;
                        this.picStep2.Image = Properties.Resources.RedX;
                        this.picStep3.Image = Properties.Resources.RedX;
                        this.picStep4.Image = Properties.Resources.RedX;

                        // sada prikazi slike i labele
                        this.picStep1.Visible = true;
                        this.picStep2.Visible = true;
                        this.picStep3.Visible = true;
                        this.picStep4.Visible = true;
                        this.labelStep1.Visible = true;
                        this.labelStep2.Visible = true;
                        this.labelStep3.Visible = true;
                        this.labelStep4.Visible = true;
                    }
                    else if (faza == "Spremanje postavki...")
                    {
                        // postavi drugu fazu slika
                        this.picStep1.Image = Properties.Resources.Check;
                        this.picStep2.Image = Properties.Resources.Loading;
                        this.picStep3.Image = Properties.Resources.RedX;
                        this.picStep4.Image = Properties.Resources.RedX;
                    }
                    else if (faza == "Inicijalizacija taska...")
                    {
                        // postavi trecu fazu slika
                        this.picStep1.Image = Properties.Resources.Check;
                        this.picStep2.Image = Properties.Resources.Check;
                        this.picStep3.Image = Properties.Resources.Loading;
                        this.picStep4.Image = Properties.Resources.RedX;
                    }
                    else if (faza == "Lansiranje!!!")
                    {
                        // postavi trecu fazu slika
                        this.picStep1.Image = Properties.Resources.Check;
                        this.picStep2.Image = Properties.Resources.Check;
                        this.picStep3.Image = Properties.Resources.Check;
                        this.picStep4.Image = Properties.Resources.Loading;
                    }
                    else if (faza == "Kreni na sljedeci prozor, a ovaj zatvori...")
                    {
                        // pokreni novi prozor za rad, a ovaj sakrij (jer ako se zatvori onda
                        // ce se izaci iz cijele aplikacije). A kad se prozor za rad zatvori,
                        // zatvori i ovo
                        var startUpWin = new Form1(this.globalPythonPath, this.globalDatabasePath);
                        this.Hide();
                        startUpWin.Closed += (s, args2) => this.Close();
                        startUpWin.Show();
                    }
                });

        }

        /// <summary>
        /// Provjera postoje li vec izradeni folderi unutar odabranog foldera za spremanje.
        /// Ako ne postoje izraduju se.
        /// </summary>
        void checkFolders()
        {
            if (!Directory.Exists(this.textBoxDBPath.Text + @"\SupersportHTMLPonuda"))
                Directory.CreateDirectory(this.textBoxDBPath.Text + @"\SupersportHTMLPonuda");
            if (!Directory.Exists(this.textBoxDBPath.Text + @"\SupersportHTMLRezultati"))
                Directory.CreateDirectory(this.textBoxDBPath.Text + @"\SupersportHTMLRezultati");
            if (!Directory.Exists(this.textBoxDBPath.Text + @"\SupersportObradenoPonuda"))
                Directory.CreateDirectory(this.textBoxDBPath.Text + @"\SupersportObradenoPonuda");
            if (!Directory.Exists(this.textBoxDBPath.Text + @"\SupersportObradenoRezultati"))
                Directory.CreateDirectory(this.textBoxDBPath.Text + @"\SupersportObradenoRezultati");
            if (!Directory.Exists(this.textBoxDBPath.Text + @"\SupersportComboPonRez"))
                Directory.CreateDirectory(this.textBoxDBPath.Text + @"\SupersportComboPonRez");

        }

        /// <summary>
        /// Spremanje postavki programa u XML dokument.
        /// </summary>
        void XMLDataSave()
        {
            if (!File.Exists(this.XMLPath) ||
                this.textBoxDBPath.Text != this.globalDatabasePath ||
                this.textBoxPythonPath.Text != this.globalPythonPath ||
                this.satiPicker.Text != this.globalExeSati ||
                this.minutePicker.Text != this.globalExeMinute)
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("DataList");
                xmlDoc.AppendChild(rootNode);

                XmlNode userNode = xmlDoc.CreateElement("Data");
                XmlAttribute attribute = xmlDoc.CreateAttribute("AttName");
                attribute.Value = "DatabasePath";
                userNode.Attributes.Append(attribute);
                userNode.InnerText = this.textBoxDBPath.Text;
                rootNode.AppendChild(userNode);

                userNode = xmlDoc.CreateElement("Data");
                attribute = xmlDoc.CreateAttribute("AttName");
                attribute.Value = "PythonPath";
                userNode.Attributes.Append(attribute);
                userNode.InnerText = this.textBoxPythonPath.Text;
                rootNode.AppendChild(userNode);

                userNode = xmlDoc.CreateElement("Data");
                attribute = xmlDoc.CreateAttribute("AttName");
                attribute.Value = "ExeSati";
                userNode.Attributes.Append(attribute);
                userNode.InnerText = this.satiPicker.Text;
                rootNode.AppendChild(userNode);

                userNode = xmlDoc.CreateElement("Data");
                attribute = xmlDoc.CreateAttribute("AttName");
                attribute.Value = "ExeMinute";
                userNode.Attributes.Append(attribute);
                userNode.InnerText = this.minutePicker.Text;
                rootNode.AppendChild(userNode);

                xmlDoc.Save(this.XMLPath);
            }
        }

        /// <summary>
        /// Stvaranje novog WindowsTaskScheduler taska.
        /// </summary>
        void createWindowsTask()
        {
            using (TaskService ts = new TaskService())
            {
                if (!ts.RootFolder.Tasks.Exists(this.imeTaska) ||
                    this.satiPicker.Text != this.globalExeSati ||
                    this.minutePicker.Text != this.globalExeMinute)
                {
                    // Create a new task definition and assign properties
                    TaskDefinition td = ts.NewTask();
                    td.RegistrationInfo.Description = "Task koji skida podatke sa kladionica.";
                    td.Settings.MultipleInstances = TaskInstancesPolicy.Parallel;
                    // Run as Administrator
                    td.Principal.RunLevel = TaskRunLevel.Highest;

                    // postavke za restart ako task ne uspije
                    td.Settings.ExecutionTimeLimit = new TimeSpan(0, 30, 0);
                    td.Settings.RestartCount = 5;
                    td.Settings.RestartInterval = new TimeSpan(0, 10, 0);

                    // Create a trigger that will fire the task at this time every other day
                    DailyTrigger daily = new DailyTrigger();
                    daily.StartBoundary = Convert.ToDateTime(DateTime.Today.ToShortDateString() +
                        " " + this.satiPicker.Text + ":" + this.minutePicker.Text + ":00");
                    daily.DaysInterval = 1;
                    td.Triggers.Add(daily);

                    // Create an action that will launch Notepad whenever the trigger fires
                    td.Actions.Add(new ExecAction(this.downloadingEXEPath));

                    // Register or replace the task in the root folder
                    ts.RootFolder.RegisterTaskDefinition(this.imeTaska, td);
                }
            }
        }

        /*LoadingWindowForm myLoadingWin;
        HtmlAgilityPack.HtmlDocument document;
        StreamWriter sr;
        HtmlWeb webGet;
        Thread myThread;

        public void HTMLAgilitySkidanje()
        {
            // path check
            if (!Directory.Exists(textBox2.Text))
            {
                MessageBox.Show("Path za ponudu ne postoji", "Greska", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Directory.Exists(textBox3.Text))
            {
                MessageBox.Show("Path za rezultate ne postoji", "Greska", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // suspend main form while downloading and show loading form
            this.SuspendLayout();
            this.myLoadingWin = new LoadingWindowForm();
            this.myLoadingWin.Show();

            ProcesSkidanja();

            // close loading form and resume main form after download
            myLoadingWin.Close();
            this.ResumeLayout();
            
        }

        public void ProcesSkidanja()
        {
            #region Ponuda obrada

            this.webGet = new HtmlWeb();

            // downloading of todays ponuda
            this.myLoadingWin.ChangeLabelValue("Skidanje ponude...");
            this.document = webGet.Load("https://www.supersport.hr/sport");

            // spremanje ponude HTML
            this.sr = new StreamWriter(Path.Combine(Properties.Settings.Default.PonudaPath,
                "HTML Ponuda " + DateTime.Now.ToString("yyyy-MM-dd") + ".txt") );
            this.sr.Write(this.document.DocumentNode.OuterHtml);
            this.sr.Close();

            // spremanje ponude .csv
            this.sr = new StreamWriter(Path.Combine(Properties.Settings.Default.PonudaPath,
                "Ponuda " + DateTime.Now.ToString("yyyy-MM-dd") + ".csv"));

            foreach (var node in this.document.DocumentNode.SelectNodes("//td"))
            {
                if (node.Attributes["class"].Value=="brojPonude")
                    this.sr.WriteLine(node.InnerText);
            }

            
            this.sr.Close();
            
            #endregion

            #region Rezultati obrada

            // downloading of todays rezultati
            this.myLoadingWin.ChangeLabelValue("Skidanje rezultata...");
            this.document = webGet.Load("https://www.supersport.hr/rezultati");

            this.sr = new StreamWriter(Path.Combine(Properties.Settings.Default.RezultatiPath,
                "HTML Rezultati " + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"));
            this.sr.Write(document.DocumentNode.OuterHtml);
            this.sr.Close();

            #endregion
        }*/
    }
}
